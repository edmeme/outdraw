Outdraw
=======
## What?

Simple command tool to render console output to an image.
It is very simple, can only handle 16 colors (foreground and background).
Capable of creating a solid background or drawing over an existing image.
It is possible to customize colors, use the #RRGGBB or #RGB formats.

Add suggestions and issues as you plese in the issue tracker.

## Example

        $ ls -al --color=always  | ./outdraw.py --solid 500x200 #111 -o img/example.png


[![example](https://bytebucket.org/edmeme/outdraw/raw/63163c12f05e2a469b996215d55f4c081b2c1b1b/img/example.png)]

