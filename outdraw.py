#!/bin/env python2
# -*- coding: utf-8 -*-

# Édgar Méndez - 2016
# Anyone can freely use, copy, edit and redistribute this program.
# Anyone can make changes to this program as long as changes are clearly stated.
# Pull requests are always preferred over forks,
#  you can also make suggestions or file issues.

import sys
import PIL
import os

from PIL import ImageColor
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw

color = [0] * 16;
# Default colors
# Black
color[0]  = "#303030"
color[8]  = "#555753"
# Red 
color[1]  = "#a40000"
color[9]  = "#EF2929"
# Green
color[2]  = "#4E9A06"
color[10] = "#8AE234"
# Yellow
color[3]  = "#C4A000"
color[11] = "#FCE94F"
# Blue
color[4]  = "#3465A4"
color[12] = "#729FCF"
# Purple
color[5]  = "#75507B"
color[13] = "#AD7FA8"
# Cyan
color[6]  = "#ce5c00"
color[14] = "#fcaf3e"
# White
color[7]  = "#babdb9"
color[15] = "#EEEEEC"


## Util

def gen(x):
    for i in x:
        yield i


def gstdin():
    buff = ""
    buff = sys.stdin.read(2000)
    while len(buff)>0:
        for c in buff:
            yield c
        buff = sys.stdin.read(2000)

def drop(g,n):
    try:
        for x in range(n):
            next(g)
        return True
    except:
        return False

def take(g,n=1,end=lambda:None ):
    b = None
    try:
        b = ""
        for x in range(n):
            b = b + next(g)
        return b
    except:
        end()
        return b


## Core
    
def write(image,text,fg,bg,font,head,transparent):
    """ 
    Draw text into an image.
    - image: destination image
    - text:  text to draw, don't support newline characters
    - fg:    foreground color
    - bg:    background color
    - head:  (x,y) tuple, marking the position where the text is drawn.
    - transparent: Can either be -1 (to have no effect) or a color index (from 0 to 15)
                   If the background color equals color[transparent], the background
                   is not drawn.
    Retunrs the width and height of the drawn text. The return value is incorrect
    if newline characters are fed.
    """
    w,h = 0,0
    if text:
        x,y = head        
        draw  = ImageDraw.Draw(image)
        w,h = font.getsize(text)

        if transparent < 0 or transparent > 15 or bg != color[transparent]:
            draw.rectangle([x,y,x+w,y+h],fill=bg)
        draw.text(head, text, fg, font=font)
    return w,h

def read():
    """ 
    Read from stdin, create an array of characters and formats.
    By far not the most optimal representation.
    Returns a list of lines, each line being a list of symbols,
    and each symbol being a tuple of a character, a foreground
    and a background colors.
    """
    i = 0
    j = 0
    fg = color[7]
    bg = color[0]
    screen_buffer = [[]]

    gin = gstdin()
    for c in gin:
        if c == '\x1B':
            if take(gin,1) == "[":
                format_spec = ""
                c = take(gin,1)
                while c!="m" and c!=None:
                    format_spec = format_spec + c
                    c = take(gin,1)
                fg,bg = format_colors(format_spec,fg,bg)
        elif c == '\n':
            j+= 1
            i = 0
            while j>=len(screen_buffer):
                screen_buffer.append([])
        elif c == '\r':
            i = 0
        elif c != None:
            screen_buffer[j].append((c,fg,bg))
            i+=1
    return screen_buffer

def format_colors(fstring,fg,bg):
    """ 
    Parses an ansi format string and picks a foreground and background colors from
    the global color palette.
    - fstring: format string, the characters after '\x1B[' and before 'm' in an ANSI
               format sequence.
    - fg, bg:  Current foreground and background.
    Returns the new foreground and background colors.
    """

    if fstring == "":
        return color[7],color[0]
    numbers = [int(x) for x in fstring.split(';')]
    for n in numbers:
        if n == 0:
            fg = color[7]
            bg = color[0]
        elif n == 39:
            fg = color[7]
        elif n == 49:
            bg = color[0]
        elif n >= 30 and n <= 37:
            fg = color[n-30]
        elif n >= 90 and n <= 97:
            fg = color[n-90+8]
        elif n >= 40 and n <= 47:
            bg = color[n-40]
        elif n >= 100 and n <= 107:
            bg = color[n-100+8]
    return fg,bg

def draw(image,head,sb,font,vspace = 0, ignore = 0):
    """
    Draws a screen buffer (as returned by read()) into an image.
    - image:  destination image
    - head:   (x,y) tuple.
    - sb:     screen buffer
    - font:   PIL font object.
    - vspace: Additional vertical space between lines.
    - ignore: backgound color index to ignore (passed to write())
    """
    baseline = head[0]
    w,h = font.getsize('O')
    vspace += h
    im_w,im_h = image.size
    image_lines = (im_h-head[1]) / vspace
    if len(sb) > image_lines:
        sb = sb[-image_lines:]
    fg = color[7]
    bg = color[0]
    line_buffer = ""
    for line in sb:
        line_buffer = ""
        for s in line:
            char,cfg,cbg = s
            if (fg != cfg or bg != cbg):
                if line_buffer != "":
                    w,h = write(image,line_buffer,fg,bg,font,head,ignore)
                    head = head[0]+w,head[1]
                    line_buffer = ""
                fg,bg = cfg,cbg
            line_buffer += char
        w,h = write(image,line_buffer,fg,bg,font,head,ignore)
        head = baseline,head[1]+vspace



## CLI
        
def print_help():
    program = sys.argv[0]
    print("usage:  %s <operation> [options]"%program)
    print("operations:")
    print("  %s {-s --solid}   <width>x<height> <color>"%program)
    print("  %s {-o --overlay} <background>"%program)
    print("")
    print("Options:")
    print("  -c, --color N=COLOR")
    print("     Set the color for the Nth terminal color index.")
    print("     N is a number from 0 to 15 and COLOR is an html-style color.")    
    print("  -i, --ignore-bg N")
    print("     If the background color is N, then the background is not drawn.")
    print("     Defaults to 0. To override this behaviour set N=-1.")
    print("  -f, --font FILE")
    print("     Set font file. Unfortunately system fonts are not supported.")
    print("     When unset, an unspecified font will be used.")
    print("  -s, --size SIZE")
    print("     Text size, in points. Defaults to 20.")
    print("  -v, --vspace SPACE")
    print("     Vertical space between lines. Defaults to 5")
    print("  -x X")
    print("     x-coordinate. Defaults to 10")
    print("  -y Y")
    print("     y-coordinate. Defaults to 10")
    print("  -o, --output")
    print("     Output file, defaults to out.png")
    print("")

## Arg parsing


def end_error():
    print("Argument list ended unexpectedly.")
    print_help()
    exit(1)

def arg_error(x):
    print(x)
    print_help()
    exit(1)

def parse_error(x):
    print(x)
    exit(2)
    
def parse_color_assig(x):
    try:
        sn,sc = x.strip().split('=')
        N = int(sn)
        sc = parse_color(sc)
        return N,sc
    except:
        parse_error("Invalid color assignment: %s"%x)

        

def parse_int(x):
    try:
        return int(x)
    except:
        parse_error("Expected an integer instead of %s"%x)

def parse_size(x):
    try:
        sw,sh = x.strip().split('x')
        W,H = int(sw),int(sh)
        if W<=0 or H<=0:
            parse_error("Width and height must be greater than 0: %s"%x)
        return W,H
    except:
        parse_error("Invalid dimensions: %s"%x)

def parse_color(x):
    x = x.strip().upper()
    if x[0] != "#":
        parse_error("Colors should start with #: %s"%x)
    for c in x[1:]:
        if c not in "ABCDEF0123456789":
            parse_error("Invalid character in color: %s"%x)
    if len(x) not in [4,7]:
        parse_error("Invalid color length: %s"%x)
    return x

def parse_fname(f,p):
    try:
        with open(f,p):
            pass
        return f        
    except:
        parse_error("Invalid file: %s"%f)

def next_arg(g):
    return take(g,end=end_error)



def main(args):
    gargs = gen(args[1:])
    image = None
    mode = next_arg(gargs)
    if mode in ["-s","--solid"]:
       width,height = parse_size(next_arg(gargs))
       back_color = parse_color(next_arg(gargs))
       image = Image.new("RGB", (width,height), back_color)
    elif mode in ["-o","--overlay"]:
       fname = parse_fname(next_arg(gargs),'r')
       image = Image.open(fname)
    else:
       arg_error("Specify a mode first, not '%s'"%mode)
    
    if image == None:
       arg_error("Cannot create or open image")

    #defaults
    ignore = 0
    font_file = None
    size = 20
    vspace = 5
    x = 10
    y = 10
    ofile = "out.png"
    
    for arg in gargs:
       if arg in ["-c","--color"]:
           N,C = parse_color_assig(next_arg(gargs))
           color[N] = C
       elif arg in ["-i","--ignore-bg"]:
           ignore = parse_int(next_arg(gargs))
       elif arg in ["-f","--font"]:
           font_file = parse_fname(next_arg(gargs),'r')
       elif arg in ["-s","--size"]:
           size = parse_int(next_arg(gargs))
       elif arg in ["-v","--vspace"]:
           vspace = parse_int(next_arg(gargs))
       elif arg  == "-x":
           x = parse_int(next_arg(gargs))
       elif arg  == "-y":
           y = parse_int(next_arg(gargs))
       elif arg in ["-o","--output"]:
           ofile = parse_fname(next_arg(gargs),'w')
       elif arg in ["-h","--help"]:
           print_help()
           exit(0)
       else:
           arg_error("Unknown argument '%s'" % arg)

    if font_file == None:
        font = ImageFont.load_default()
    else:
        font = ImageFont.truetype(font_file,size,encoding='unic')
    
    screen_buffer = read()
    draw(image,(x,y),screen_buffer,font,vspace,ignore)
    image.save(ofile)
    
    return 0



if __name__=="__main__":
    exit(main(sys.argv))
